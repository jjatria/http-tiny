#!/usr/bin/env raku

use Test;
use HTTP::Tiny;

unless %*ENV<ONLINE_TESTING> {
    say '1..0 # SKIP: ONLINE_TESTING not set';
    exit;
}

my $ua = HTTP::Tiny.new: :throw-exceptions;
my $tests = 10;

for < http https > -> $scheme {
    subtest "{ $scheme.uc } tests" => {
        if $scheme eq 'https' && !HTTP::Tiny.can-ssl {
            skip 'No HTTPS support';
        }
        else {
            my $c = Channel.new;

            for ^$tests {
                start {
                    my $res = $ua.get: "$scheme://httpbin.org/status/200";
                    $c.send: $_ => $res;
                }
            }

            while $c.receive -> ( :key($i), :value($res) ) {
                ok $res<success>, "Finished request $i" or do {
                    note $res< status reason >.join: ' ';
                    note $res<content>.decode;
                }
                last if ++$ == 10;
            }
        }
    }
}

done-testing;
